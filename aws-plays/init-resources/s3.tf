resource "aws_s3_bucket" "plays_jar_bucket" {
  bucket = var.s3_bucket_name
  acl = "private"
  policy = data.aws_iam_policy_document.s3_access_policy.json
  region = var.region

  versioning {
    enabled = true
  }
}

data "aws_iam_policy_document" "s3_access_policy" {
  version = "2012-10-17"
  statement {
    effect = "Allow"
    actions = [
      "s3:ListBucket",
      "s3:GetObject",
      "s3:PutObject",
      "s3:DeleteObject"]
    resources = [
      "arn:aws:s3:::${var.s3_bucket_name}",
      "arn:aws:s3:::${var.s3_bucket_name}/*"]
    principals {
      identifiers = ["AWS", "Lambda"]
      type = "*"
    }
  }
}