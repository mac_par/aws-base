variable "owner" {
  type = string
  default = "maciej.prandyk@yahoo.com"
}

variable "region" {
  type = string
  default = "eu-west-1"
}

variable "s3_bucket_name" {
  type = string
  default = "plays-jar-store"
}