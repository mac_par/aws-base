terraform {
  required_version = ">= 0.12, < 0.13"
  backend "s3" {
    region = "eu-west-1"
    bucket = "terraform-state-mkbucket"
    key = "aws-plays-init/tfstate.tf"
  }
}

provider "aws" {
  region = var.region
}